package ru.tsc.avramenko.tm.api.service;

import org.jetbrains.annotations.NotNull;

public interface IConnectionSettings {

    @NotNull
    String getJdbcUser();

    @NotNull
    String getJdbcPass();

    @NotNull
    String getJdbcUrl();

    @NotNull
    String getJdbcDriver();

    @NotNull
    String getHibernateDialect();

    @NotNull
    String getHibernateHbm2ddl();

    @NotNull
    String getHibernateShowSql();

    @NotNull
    String getHibernateCacheUseSecondLevelCache();

    @NotNull
    String getHibernateCacheUseQueryCache();

    @NotNull
    String getHibernateCacheUseMinimalPuts();

    @NotNull
    String getHibernateCacheHazelcastUseLiteMember();

    @NotNull
    String getHibernateCacheRegionPrefix();

    @NotNull
    String getHibernateCacheProviderConfigurationFileResourcePath();

    @NotNull
    String getHibernateCacheRegionFactoryClass();

}