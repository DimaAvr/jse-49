package ru.tsc.avramenko.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.tsc.avramenko.tm.component.Bootstrap;
import ru.tsc.avramenko.tm.marker.SoapCategory;

public class UserEndpointTest {

    @NotNull
    protected static final Bootstrap bootstrap = new Bootstrap();

    @Test
    @Category(SoapCategory.class)
    public void existsUserByEmail() {
        @NotNull final boolean user = bootstrap.getUserEndpoint().existsUserByEmail("EmailNewTest1");
        Assert.assertTrue(user);
    }

    @Test
    @Category(SoapCategory.class)
    public void existsUserByLogin() {
        @NotNull final boolean user = bootstrap.getUserEndpoint().existsUserByLogin("User");
        Assert.assertTrue(user);
    }

}