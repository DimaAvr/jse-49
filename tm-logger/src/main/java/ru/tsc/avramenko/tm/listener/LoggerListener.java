package ru.tsc.avramenko.tm.listener;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import ru.tsc.avramenko.tm.api.ILoggingService;
import ru.tsc.avramenko.tm.service.LoggingService;

import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.TextMessage;

@NoArgsConstructor
public class LoggerListener implements MessageListener {

    @NotNull
    final ILoggingService loggingService = new LoggingService();

    @Override
    public void onMessage(@NotNull final Message message) {
        if (message instanceof TextMessage) {
            loggingService.writeLog((TextMessage) message);
        }
    }

}